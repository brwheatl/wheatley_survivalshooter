﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;
    bool poweredUp = false;
    float powerUpTimer = 0f;
    bool isDashing = false;
    bool fadeIn = true;

    public PlayerShooting playerShooting;
    public int dashSpeed = 2;
    public float dashTime;
    public Text dashText;

    private void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent <Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    private void Move (float h, float v)
    {
        movement.Set(h, 0f, v);

        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        /*Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
        */

        //print("H:" + Input.GetAxis("HorizontalTurn") + " V: " + Input.GetAxis("VerticalTurn"));
        Vector3 inputVect = new Vector3(Input.GetAxis("HorizontalTurn"), 0f, Input.GetAxis("VerticalTurn"));
        if(inputVect.magnitude >= 0.2f)
        {
            Quaternion newRotation = Quaternion.LookRotation(inputVect);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }

    private void Update()
    {

        if (poweredUp)
        {
            powerUpTimer -= Time.deltaTime;
            if (powerUpTimer <= 0f)
            {
                playerShooting.timeBetweenBullets = 0.15f;
            }
        }

        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
            print("Number of Enemies: " + enemyArray.Length);
        }*/

        if (Input.GetButtonDown("Dash") && isDashing == false)
        {
            Dash();
            isDashing = true;
            fadeIn = false;
            AnimateDashText();
        }

        if (isDashing == true)
        {
            dashTime += Time.deltaTime;
            if (dashTime >= 2)
            {
                isDashing = false;
                speed = 6f;
                dashTime = 0f;
                fadeIn = true;
                AnimateDashText();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pickup") && !poweredUp)
        {
            poweredUp = true;
            powerUpTimer = 3f;
            playerShooting.timeBetweenBullets = 0.06f;
            Destroy(other.gameObject);
        }
    }

    void Dash()
    {
        speed = speed * dashSpeed;
    }

    void AnimateDashText()
    {
        if (fadeIn == false)
        {
            dashText.color = new Color(1f, 1f, 1f, Mathf.MoveTowards(dashText.color.a, 0f, Time.timeScale * .5f));
        }
        else
        {
            dashText.color = new Color(1f, 1f, 1f, Mathf.MoveTowards(dashText.color.a, 1f, Time.timeScale * .5f));
        }
    }
}
