﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    public GameObject grenadePrefab;
    public Transform gunBarrel;
    public float grenadeForce;
    public Text grenadeText;
    public Text ammoText;

    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    bool isGrenade = false;
    float grenadeTime;
    bool fadeIn = true;
    float fadeTime;
    int ammoCount = 20;
    bool canShoot = true;
    float reloadTime;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && canShoot == true)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }

        if (Input.GetButtonDown("Fire2") && isGrenade == false)
        {
            ThrowGrenade();
            isGrenade = true;
            fadeIn = false;
            AnimateGrenadeText();
        }

        if (isGrenade == true)
        {
            grenadeTime += Time.deltaTime;
            if (grenadeTime >= 4f)
            {
                isGrenade = false;
                grenadeTime = 0f;
                fadeIn = true;
                AnimateGrenadeText();
            }
        }

        if (ammoCount <= 0)
        {
            reloadTime += Time.deltaTime;
            canShoot = false;
            if (reloadTime >= 1)
            {
                ammoCount = 12;
                canShoot = true;
                reloadTime = 0f;
            }
        }
        ammoText.text = "Ammo: " + ammoCount;
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f;

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop ();
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        ammoCount -= 1;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }

    void ThrowGrenade()
    {
        GameObject grenadeClone = Instantiate(grenadePrefab, gunBarrel.position, grenadePrefab.transform.rotation) as GameObject;
        grenadeClone.GetComponent<Rigidbody>().AddForce(gunBarrel.forward * grenadeForce);
        grenadeClone.GetComponent<Rigidbody>().AddForce(gunBarrel.up * grenadeForce);
    }

    void AnimateGrenadeText()
    {
        if (fadeIn == false)
        {
            grenadeText.color = new Color(1f, 1f, 1f, Mathf.MoveTowards(grenadeText.color.a, 0f, Time.timeScale * .5f));
        }
        else
        {
            grenadeText.color = new Color(1f, 1f, 1f, Mathf.MoveTowards(grenadeText.color.a, 1f, Time.timeScale * .5f));
        }
    }
}
