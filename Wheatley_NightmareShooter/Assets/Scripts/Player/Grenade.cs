﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour {

    public Rigidbody grenadePrefab;
    public float speed = 10f;
    public Transform gunBarrel;
    public SphereCollider grenadeAOE;
    public AudioSource explodeClip;

	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {

	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Floor") || collision.collider.CompareTag("Enemy"))
        {
            Explode();
        }
    }

    void Explode()
    {
        Instantiate(grenadeAOE, transform.position, transform.rotation);
        Invoke("EndExplode", 3);
        explodeClip.Play();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
    }

    void EndExplode()
    {
        Destroy(gameObject);
    }
}
