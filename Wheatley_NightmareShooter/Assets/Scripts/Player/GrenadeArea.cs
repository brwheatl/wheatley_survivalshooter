﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeArea : MonoBehaviour {

    public int grenadeDamage = 40;

    float endTime = 0.5f;

	// Use this for initialization
	void Start () {
        Invoke("EndExplode", endTime);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void EndExplode()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            EnemyHealth eh = other.GetComponent<EnemyHealth>();
            eh.TakeDamage(grenadeDamage, other.transform.position);
        }
    }
}
